export function openPopup() {
  return {
    type: "OPEN_POPUP",
  };
}

export function closePopup() {
  return {
    type: "CLOSE_POPUP",
  };
}
