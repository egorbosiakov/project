import Subheading from "../Subheading/Subheading";
import "./Gallery.scss";
import forestRoad from "../../Assets/Images/GalleryImage/forestRoad.jpeg";
import ridge from "../../Assets/Images/GalleryImage/ridge.jpeg";
import Road from "../../Assets/Images/GalleryImage/Road.jpeg";
import smoothRoad from "../../Assets/Images/GalleryImage/smoothRoad.jpeg";
import tower from "../../Assets/Images/GalleryImage/tower.jpeg";
import waterfall from "../../Assets/Images/GalleryImage/waterfall.jpeg";
import windingRoad from "../../Assets/Images/GalleryImage/windingRoad.jpeg";
import Button from "../Button/Button";
const Gallery = () => {
  return (
    <section className="gallery">
      <div className="gallery__wrapper">
        <Subheading
          heading="Удивительные Места"
          text="Lorem ipsum dolor sit amet, consectetur adipiscing elit nullam nunc justo sagittis suscipit ultrices."
        />
        <div className="gallery__container">
          <div className="gallery__block">
            <img className="gallery__img" src={forestRoad} alt="road"></img>
            <img className="gallery__img" src={waterfall} alt="waterfall"></img>
          </div>
          <div className="gallery__block">
            <img
              className="gallery__img"
              id="centr"
              src={ridge}
              alt="ridge"
            ></img>
            <img
              className="gallery__img"
              id="centr"
              src={tower}
              alt="tower"
            ></img>
            <img
              className="gallery__img"
              id="centr"
              src={smoothRoad}
              alt="road"
            ></img>
          </div>
          <div className="gallery__block gallery__block_right">
            <img className="gallery__img" src={Road} alt="road"></img>
            <img className="gallery__img" src={windingRoad} alt="road"></img>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Gallery;
