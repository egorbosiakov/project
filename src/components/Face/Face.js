import "./Face.scss";
import Button from "../Button/Button";
import aerostat from "../../Assets/Images/FaceImage/aerostat.png";
import { PureComponent } from "react";
import { openPopup } from "../../actionCreators.js";
import { connect } from "react-redux";

class Face extends PureComponent {
  clickPopup = () => {
    const { openPopup } = this.props;
    openPopup();
  };

  render() {
    return (
      <section className="face">
        <div className="face__wrapper">
          <div className="face__blot">
            <h4 className="face__suptext">пришло время для великого</h4>
            <h1 className="face__title">Приключение</h1>
            <Button text="начать сейчас" onClick={this.clickPopup} />
            <img className="face__img" src={aerostat}></img>
          </div>
        </div>
      </section>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  openPopup: () => dispatch(openPopup()),
});

export default connect(null, mapDispatchToProps)(Face);
