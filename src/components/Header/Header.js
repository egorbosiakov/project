import "./Header.scss";

const Header = () => {
  return (
    <header className="header">
      <div className="header__wrapper">
        <h3 className="header__title">туристическое агенство</h3>
      </div>
    </header>
  );
};

export default Header;
