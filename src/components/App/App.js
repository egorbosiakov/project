import { Fragment, PureComponent } from "react";
import "./App.scss";
import { connect } from "react-redux";
import Header from "../Header/Header";
import Face from "../Face/Face";
import Info from "../Info/Info";
import Directions from "../Directions/Directions";
import Gallery from "../Gallery/Gallery";
import Statistics from "../Statistics/Statistics";
import Footer from "../Footer/Footer";
import Popup from "../Popup/Popup";
import Form from "../Form/Form";

class App extends PureComponent {
  render() {
    const { showPopup } = this.props;
    return (
      <Fragment>
        {showPopup && <Popup />}
        <Header />
        <Face />
        <Info />
        <Directions />
        <Gallery />
        <Form />
        <Statistics />
        <Footer />
      </Fragment>
    );
  }
}
const mapStateToProps = ({ popup }) => ({
  showPopup: popup.showPopup,
});

export default connect(mapStateToProps)(App);
