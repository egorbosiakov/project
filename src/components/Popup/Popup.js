import "./Popup.scss";
import { connect } from "react-redux";
import { PureComponent } from "react";
import { closePopup } from "./../../actionCreators";

class Popup extends PureComponent {
  clickPopup = () => {
    const { closePopup } = this.props;
    closePopup();
  };

  render() {
    return (
      <div className="popup">
        <div className="popup__wrapper">
          <p>это заготовка попапа</p>
          <button className="popup__close" onClick={this.clickPopup}></button>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  closePopup: () => dispatch(closePopup()),
});

export default connect(null, mapDispatchToProps)(Popup);
