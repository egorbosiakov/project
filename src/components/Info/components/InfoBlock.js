import Line from "../../Line/Line";
import "./InfoBlock.scss";

const InfoBlock = ({ img, title, text }) => {
  return (
    <div className="infoBlock">
      <img src={img} className="infoBlock__img"></img>
      <h3 className="infoBlock__title">{title}</h3>
      <p className="infoBlock__text">{text}</p>
      <Line />
    </div>
  );
};

export default InfoBlock;
