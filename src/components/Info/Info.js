import InfoBlock from "../Info/components/InfoBlock";
import orbita from "../../Assets/Images/InfoImage/orbita.svg";
import keys from "../../Assets/Images/InfoImage/keys.svg";
import mount from "../../Assets/Images/InfoImage/mount.svg";
import astronaut from "../../Assets/Images/InfoImage/astronaut.svg";
import "./Info.scss";
const Info = () => {
  const arrayInfo = [
    {
      img: orbita,
      title: "Типы Праздников",
      text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam.",
    },
    {
      img: keys,
      title: "Путеводители",
      text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam.",
    },
    {
      img: mount,
      title: "Экстремальные Туры",
      text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam.",
    },
    {
      img: astronaut,
      title: "Отпуск Мечты",
      text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam.",
    },
  ];

  return (
    <section className="info">
      <div className="info__wrapper">
        {arrayInfo.map((item) => (
          <InfoBlock
            key={item.id}
            img={item.img}
            title={item.title}
            text={item.text}
          />
        ))}
      </div>
    </section>
  );
};

export default Info;
