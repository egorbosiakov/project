import FooterBlock from "./components/FooterBlock";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer__wrapper">
        <div className="footer__wrapperInner">
          <FooterBlock />
          <FooterBlock />
          <FooterBlock />
        </div>
        <hr className="footer__line"></hr>
        <h3 className="footer__title">Туристическая Компания</h3>
      </div>
    </footer>
  );
};
export default Footer;
