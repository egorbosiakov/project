import "./FooterBlock.scss";

const FooterBlock = () => {
  return (
    <div className="footerBlock">
      <h3 className="footerBlock__title">Заголовок</h3>
      <p className="footerBlock__text">Образец текста нижнего колонтитула</p>
    </div>
  );
};

export default FooterBlock;
