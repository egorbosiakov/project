import Button from "../Button/Button";
import Subheading from "../Subheading//Subheading";
import "./Statistics.scss";
import StatisticBlock from "../Statistics/components/StatisticsBlock";

const Statistics = () => {
  const arrayStatistics = [
    {
      title: "ГОРЯЧИЕ НАПРАВЛЕНИЯ",
      number: "37",
    },
    {
      title: "ДОВОЛЬНЫЕ КЛИЕНТЫ",
      number: "677",
    },
    {
      title: "ЧАШКИ КОФЕ",
      number: "87",
    },
    {
      title: "ПОСЕТИЛИ СТРАНЫ",
      number: "107",
    },
  ];

  return (
    <section className="statistics">
      <div className="statistics__wrapper">
        <Subheading
          heading="Помогая Мечтам Исполняться"
          text="Вы можете получить от нас столько помощи, сколько захотите. Тебе решать. Наконец, вы должны знать, что мы относимся к местным жителям с уважением и справедливостью. Это окупается загрузкой ведра, потому что заботливые местные жители позволяют ближе к их культуре, их людям и их природе. Что хорошо для них и хорошо для вас. Кроме того, если вы хотите, при бронировании отпуска мы оплатим однодневную поездку для малообеспеченного ребенка из развивающейся страны. в игровой парк, гору или музей и т. д. Мы называем это ответственным туризмом."
        />
        <Button text="связаться" />
        <div className="statistics__wrapperInner">
          {arrayStatistics.map((item) => (
            <StatisticBlock
              key={item.id}
              title={item.title}
              number={item.number}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default Statistics;
