import "./StatisticsBlock.scss";

const StatisticsBlock = ({ title, number }) => {
  return (
    <div className="statisticsBlock">
      <h3 className="statisticsBlock__title">{title}</h3>
      <p className="statisticsBlock__number">{number}</p>
    </div>
  );
};

export default StatisticsBlock;
