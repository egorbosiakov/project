import Input from "./components/Input";
import Button from "./../Button/Button";
import "./Form.scss";
import { PureComponent } from "react";

class Form extends PureComponent {
  state = {
    isLoading: false,
    isShowForm: true,
    email: "",
    name: "",
    error: "",
  };

  handleChangeName = (e) => {
    this.setState({ name: e.target.value });
  };

  handleChangeEmail = (e) => {
    this.setState({ email: e.target.value });
  };

  handleClick = async () => {
    this.setState({ isLoading: true });
    try {
      await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
          email: this.state.email,
          name: this.state.name,
        }),
      });
      this.setState({ isLoading: false });
      this.setState({ isShowForm: false });
    } catch (err) {
      this.setState({ isLoading: false });
    }
  };

  render() {
    const { isShowForm, email, name, isLoading } = this.state;
    return (
      <section className="form">
        {isShowForm ? (
          <div className="container">
            <form className="form__wrapper">
              <Input
                type="text"
                name="name"
                placeholder="Enter your Name"
                onChange={this.handleChangeName}
                value={name}
              />
              <Input
                type="email"
                name="email"
                placeholder="Enter your email"
                onChange={this.handleChangeEmail}
                value={email}
              />
              <Button
                className="button_orange button_orangeForm"
                text="представить"
                onClick={this.handleClick}
                disabled={!name || (!email && isLoading)}
              />
            </form>
          </div>
        ) : (
          "МЫ С ВАМИ СВЯЖЕМСЯ"
        )}
      </section>
    );
  }
}

export default Form;
