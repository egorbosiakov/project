import "./Input.scss";

const Input = ({ type, placeholder, value, onChange, name }) => {
  return (
    <input
      name={name}
      className="input"
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    ></input>
  );
};

export default Input;
