import "./DirectionsBlock.scss";

const DirectionsBlock = ({ img, title, text }) => {
  return (
    <div className="directionBlock">
      <img src={img} className="directionBlock__img"></img>
      <h3 className="directionBlock__title">{title}</h3>
      <p className="directionBlock__text">{text}</p>
    </div>
  );
};

export default DirectionsBlock;
