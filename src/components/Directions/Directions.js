import Subheading from "../Subheading/Subheading";
import DirectionsBlock from "../Directions/components/DirectionsBlock";
import mountainRoad from "../../Assets/Images/DirectionsImage/mountainRoad.jpeg";
import roofs from "../../Assets/Images/DirectionsImage/roofs.jpeg";
import bike from "../../Assets/Images/DirectionsImage/bike.jpeg";
import camel from "../../Assets/Images/DirectionsImage/camel.jpeg";
import "./Directions.scss";
import Button from "../Button/Button";

const Directions = () => {
  const arrayDirections = [
    {
      img: mountainRoad,
      title: "300 £ + Скидки",
      text: "От древних культур до удивительных пейзажей - найдите самые лучшие скидки",
    },
    {
      img: roofs,
      title: "Праздники Испании",
      text: "Высокие горы, залитые солнцем побережья, мавританское наследие и более изысканная кухня",
    },
    {
      img: bike,
      title: "Велосипедные Каникулы",
      text: "Пробудите давно потерянное в детстве чувство свободы или бросьте вызов приключениям",
    },
    {
      img: camel,
      title: "Праздники Африки",
      text: "Экзотические базары, древние чудеса, уникальная дикая природа и огромные песчаные дюны в бесконечных пустынях.",
    },
  ];

  return (
    <section className="directions">
      <div className="directions__wrapper">
        <Subheading
          heading="Горячие Направления"
          text="Первое место для поиска экологически чистого отдыха"
        />
        <div className="directions__wrapperInner">
          {arrayDirections.map((item) => (
            <DirectionsBlock
              key={item.id}
              img={item.img}
              title={item.title}
              text={item.text}
            />
          ))}
          <Button text="показать больше" />
        </div>
        <div className="directions__line"></div>
      </div>
    </section>
  );
};

export default Directions;
