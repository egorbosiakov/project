import "./Subheading.scss";
import Line from "../Line/Line";

const Subheading = ({ heading, text }) => {
  return (
    <div className="subheading">
      <h2 className="subheading__title">{heading}</h2>
      <Line id="subheadin__line" />
      <p className="subheading__text">{text}</p>
    </div>
  );
};

export default Subheading;
