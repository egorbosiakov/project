import { createStore } from "redux";
import reducer from "./Reducers/rootReducer.js";

const store = createStore(reducer);

export default store;
