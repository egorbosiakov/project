const initState = {
  showPopup: false,
};

function reducer(state = initState, action) {
  switch (action.type) {
    case "OPEN_POPUP": {
      return { ...state, showPopup: true };
    }
    case "CLOSE_POPUP": {
      return { ...state, showPopup: false };
    }

    default:
      return state;
  }
}

export default reducer;
