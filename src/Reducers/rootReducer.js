import { combineReducers } from "redux";
import popup from "./popupReducer";

export default combineReducers({
  popup,
});
